class Tabs {
    static DEFAULT_TAB_ITEM = 0;
    static TABS_ROOT_CLASS = "tabs";
    static TABS_NAV_CLASS = "tabs__nav";
    static TABS_NAV_ACTIVE_CLASS = "tabs__nav--active";
    static TABS_ITEM_CLASS = "tabs__item";
    static TABS_ITEM_SHOW_CLASS = "tabs__item--show";

    constructor(rootEl, defaultOpenIndex = Tabs.DEFAULT_TAB_ITEM) {
        this.rootEl = rootEl;
        this.currentTab = defaultOpenIndex;
        this.tabNavs = Array.from(this.rootEl.children[0].children);
        this.tabPanels = Array.from(this.rootEl.children[1].children);

        this.bindStyles();
        this.bindEvents();
        this.showActiveTab();
    }

    bindStyles() {
        this.rootEl.classList.add(Tabs.TABS_ROOT_CLASS);

        this.tabNavs.forEach((tabNav) => {
            tabNav.classList.add(Tabs.TABS_NAV_CLASS);
        })

        this.tabPanels.forEach((tabPane) => {
            tabPane.classList.add(Tabs.TABS_ITEM_CLASS);
        })
    }

    bindEvents() {
        this.rootEl.addEventListener('click', this.onRootElClick.bind(this));
    }

    onRootElClick(event) {
        const tabNavElement = event.target.closest(`.${Tabs.TABS_NAV_CLASS}`);

        if(!tabNavElement) {
            return;
        }

        const nextTabIndex = this.getTabNavIndex(tabNavElement);

        if(nextTabIndex !== this.currentTab) {
            this.removeActiveTab();
            this.currentTab = nextTabIndex;
            this.showActiveTab();
        }
    }

    getTabNavIndex(tabNavEl) {
        return this.tabNavs.indexOf(tabNavEl);
    }

    showActiveTab() {
        this.tabNavs[this.currentTab].classList.add(Tabs.TABS_NAV_ACTIVE_CLASS);
        this.tabPanels[this.currentTab].classList.add(Tabs.TABS_ITEM_SHOW_CLASS);
    }

    removeActiveTab() {
        this.tabNavs[this.currentTab].classList.remove(Tabs.TABS_NAV_ACTIVE_CLASS);
        this.tabPanels[this.currentTab].classList.remove(Tabs.TABS_ITEM_SHOW_CLASS);
    }
}
